#ifndef DUNE_VTKDIFF_VTKREADER
#define DUNE_VTKDIFF_VTKREADER

#include <vtkSmartPointer.h>
#include <vtkXMLReader.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLPUnstructuredGridReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkXMLRectilinearGridReader.h>
#include <vtkXMLHyperOctreeReader.h>
#include <vtkXMLCompositeDataReader.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkXMLImageDataReader.h>
#include <vtkDataSetReader.h>
#include <vtkDataSet.h>
#include <vtkUnstructuredGrid.h>
#include <vtkRectilinearGrid.h>
#include <vtkHyperOctree.h>
#include <vtkImageData.h>
#include <vtkPolyData.h>
#include <vtkStructuredGrid.h>
#include <vtkCell.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkFieldData.h>
#include <vtkCellTypes.h>

#include <vtkCellLocator.h>
#include <vtkCellTreeLocator.h>

#include <vtkGenericCell.h>

#include<map>

class VTKReader
{
  private:

    mutable vtkSmartPointer<vtkDataSet> dataSet;
    mutable vtkSmartPointer<vtkCellTreeLocator> cellTreeLocator;
    
    mutable vtkSmartPointer<vtkGenericCell> genericCell;
    mutable vtkIdType cellId;
    mutable double bounds[6];
    mutable double pcoords[3];
    mutable double weights[10];
    mutable double tuple[10];

    const std::string fileName;
    std::string type;
    int arrayId;
    mutable vtkDataArray* dataArray;
    mutable int numberOfComponents;

    mutable double closestPoint[3];
    mutable int subId;
    mutable double dist2;

    const double threshold;

  public:

    VTKReader(std::string fileName_, std::string type_ = "vertex", int arrayId_ = 0,
        double threshold_ = std::numeric_limits<double>::max())
      : fileName(fileName_), type(type_), arrayId(arrayId_), threshold(threshold_)
    {
      std::cout << "=== reading in VTK file " << fileName << " ===" << std::endl;

      std::string::size_type idx = fileName.rfind('.');

      std::string extension;
      if(idx != std::string::npos)
        extension = fileName.substr(idx);
      else
        DUNE_THROW(Dune::Exception,"VTK filename is missing extension");

      // Dispatch based on file extension
      if (extension == ".vtu")
        dataSet = ReadXMLFile<vtkXMLUnstructuredGridReader>(fileName);
      else if (extension == ".pvtu")
        dataSet = ReadXMLFile<vtkXMLPUnstructuredGridReader>(fileName);
      else if (extension == ".vtp")
        dataSet = ReadXMLFile<vtkXMLPolyDataReader>(fileName);
      else if (extension == ".vts")
        dataSet = ReadXMLFile<vtkXMLStructuredGridReader>(fileName);
      else if (extension == ".vtr")
        dataSet = ReadXMLFile<vtkXMLRectilinearGridReader>(fileName);
      else if (extension == ".vti")
        dataSet = ReadXMLFile<vtkXMLImageDataReader>(fileName);
      else if (extension == ".vto")
        dataSet = ReadXMLFile<vtkXMLHyperOctreeReader>(fileName);
      else if (extension == ".vtk")
        dataSet = ReadXMLFile<vtkDataSetReader>(fileName);
      else
        DUNE_THROW(Dune::Exception,"Unknown VTK extension " + extension);

      dataSet->GetBounds(bounds);

      cellTreeLocator = vtkSmartPointer<vtkCellTreeLocator>::New();
      cellTreeLocator->SetDataSet(dataSet);
      cellTreeLocator->BuildLocator();

      genericCell = vtkSmartPointer<vtkGenericCell>::New();

      setArrayTypeAndId(type_,arrayId_);

      report();
    }

    void setArrayTypeAndId(std::string type_, int arrayId_)
    {
      type = type_;
      arrayId = arrayId_;

      std::cout << "type is " << type << ", id is " << arrayId << std::endl;
      if (type == "cell")
      {
        if (arrayId >= dataSet->GetCellData()->GetNumberOfArrays())
          DUNE_THROW(Dune::Exception,"array index too large");
        dataArray = dataSet->GetCellData()->GetArray(arrayId);
      }
      else if (type == "vertex")
      {
        if (arrayId >= dataSet->GetPointData()->GetNumberOfArrays())
          DUNE_THROW(Dune::Exception,"array index too large");
        dataArray = dataSet->GetPointData()->GetArray(arrayId);
      }
      else
        DUNE_THROW(Dune::Exception,"VTK data type " + type + "not understood");
      numberOfComponents = dataArray->GetNumberOfComponents();
      std::cout << "type is " << type << ", id is " << arrayId
        << ", number of components " << numberOfComponents << std::endl;
    }

    std::string getType() const
    {
      return type;
    }

    int getArrayId() const
    {
      return arrayId;
    }

    int getNumberOfComponents() const
    {
      return numberOfComponents;
    }

    const double * getBounds() const
    {
      return bounds;
    }

    bool eval(double coords[], double data[]) const
    {
      if (!(genericCell->EvaluatePosition(coords,closestPoint,subId,pcoords,dist2,weights)==1))
        cellId = cellTreeLocator->FindCell(coords,0,genericCell,pcoords,weights);

      if (cellId >= 0)
      {
        if (type == "vertex")
        {
          for (int i = 0; i < numberOfComponents; i++)
            data[i] = 0.;

          for (int i = 0; i < genericCell->GetNumberOfPoints(); i++)
          {
            dataArray->GetTuple(genericCell->GetPointId(i),tuple);
          
            for (int j = 0; j < numberOfComponents; j++)
              data[j] += weights[i] * tuple[j];
          }
        }
        else
        {
          dataArray->GetTuple(cellId,tuple);
          for (int j = 0; j < numberOfComponents; j++)
            data[j] = tuple[j];
        }

        for (int i = 0; i < numberOfComponents; i++)
          if (data[i] < -threshold || data[i] > threshold)
            return false;

        return true;
      }

      return false;
    }

    void report() const
    {
      int numberOfCells = dataSet->GetNumberOfCells();
      int numberOfPoints = dataSet->GetNumberOfPoints();

      // Generate a report
      std::cout << "------------------------" << std::endl;
      std::cout << "dataset contains a "
        << dataSet->GetClassName()
        <<  " that has " << numberOfCells << " cells"
        << " and " << numberOfPoints << " points." << std::endl;
      typedef std::map<int,int> CellContainer;
      CellContainer cellMap;
      for (int i = 0; i < numberOfCells; i++)
      {
        cellMap[dataSet->GetCellType(i)]++;
      }

      CellContainer::const_iterator it = cellMap.begin();
      while (it != cellMap.end())
      {
        std::cout << "\tCell type "
          << vtkCellTypes::GetClassNameFromTypeId(it->first)
          << " occurs " << it->second << " times." << std::endl;
        ++it;
      }

      // Now check for point data
      vtkPointData* pd = dataSet->GetPointData();
      if (pd)
      {
        std::cout << " contains point data with "
          << pd->GetNumberOfArrays()
          << " arrays." << std::endl;
        for (int i = 0; i < pd->GetNumberOfArrays(); i++)
        {
          std::cout << "\tArray " << i
            << " is named "
            << (pd->GetArrayName(i) ? pd->GetArrayName(i) : "NULL")
            << std::endl;

          int numberOfTuples     = pd->GetArray(i)->GetNumberOfTuples();
          int numberOfComponents = pd->GetArray(i)->GetNumberOfComponents();
          std::cout << "\tArray " << i
            << " has " << numberOfTuples << " tuples with "
            << numberOfComponents << " components" << std::endl;
        }
      }
      // Now check for cell data
      vtkCellData* cd = dataSet->GetCellData();
      if (cd)
      {
        std::cout << " contains cell data with "
          << cd->GetNumberOfArrays()
          << " arrays." << std::endl;
        for (int i = 0; i < cd->GetNumberOfArrays(); i++)
        {
          std::cout << "\tArray " << i
            << " is named "
            << (cd->GetArrayName(i) ? cd->GetArrayName(i) : "NULL")
            << std::endl;

          int numberOfTuples     = cd->GetArray(i)->GetNumberOfTuples();
          int numberOfComponents = cd->GetArray(i)->GetNumberOfComponents();
          std::cout << "\tArray " << i
            << " has " << numberOfTuples << " tuples with "
            << numberOfComponents << " components" << std::endl;
        }
      }
      // Now check for field data
      if (dataSet->GetFieldData())
      {
        std::cout << " contains field data with "
          << dataSet->GetFieldData()->GetNumberOfArrays()
          << " arrays." << std::endl;
        for (int i = 0; i < dataSet->GetFieldData()->GetNumberOfArrays(); i++)
        {
          std::cout << "\tArray " << i
            << " is named " << dataSet->GetFieldData()->GetArray(i)->GetName()
            << std::endl;
        }
      }
      std::cout << "------------------------" << std::endl;
    }

  private:

    template<class ReaderType>
      vtkSmartPointer<vtkDataSet> ReadXMLFile(std::string fileName) const
      {
        vtkSmartPointer<ReaderType> reader(vtkSmartPointer<ReaderType>::New());
        reader->SetFileName(fileName.c_str());
        reader->Update();
        reader->GetOutput()->Register(reader);
        return reader->GetOutput();
      }

};

#endif // DUNE_VTKDIFF_VTKREADER
