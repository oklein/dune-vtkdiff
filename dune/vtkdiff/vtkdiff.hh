#ifndef DUNE_VTKDIFF_VTKDIFF
#define DUNE_VTKDIFF_VTKDIFF

#include "dune/vtkdiff/vtkreader.hh"
#include "dune/vtkdiff/vtkwriter.hh"

class VTKDiff
{
  private:

    const VTKReader& firstVTK, secondVTK;
    double meshwidth;
    double bounds[6];
    double steps[3];

  public:

    VTKDiff(const VTKReader& firstVTK_, const VTKReader& secondVTK_, double meshwidth_)
      : firstVTK(firstVTK_), secondVTK(secondVTK_), meshwidth(meshwidth_)
    {
      for (int i = 0; i < 6; i++)
      {
        if (std::abs(firstVTK.getBounds()[i] - secondVTK.getBounds()[i]) > 1e-6)
          DUNE_THROW(Dune::Exception,"bounds of VTK files don't match!");

        bounds[i] = firstVTK.getBounds()[i];
      }

      for (int i = 0; i < 3; i++)
        steps[i] = (bounds[2*i+1]-bounds[2*i])/meshwidth;
    }

    double two_norm(int dim) const
    {
      double coords[3];
      for (int i = 0; i < 3; i++)
        coords[i] = bounds[2*i+1] - bounds[2*i];

      double sum = 0;
      int numComp = firstVTK.getNumberOfComponents();
      double* firstValue  = new double[numComp];
      double* secondValue = new double[numComp];
      double volume = std::pow(meshwidth,dim);
      if (dim == 2)
      {
        int count = 0;
        for (coords[1] = bounds[2]+ 0.5*meshwidth; coords[1] < bounds[3]; coords[1] += meshwidth)
          for (coords[0] = bounds[0]+ 0.5*meshwidth; coords[0] < bounds[1]; coords[0] += meshwidth)
          {
            if (!firstVTK.eval(coords,firstValue))
              continue;
            if (!secondVTK.eval(coords,secondValue))
              continue;
            for (int i = 0; i < numComp; i++)
              sum += std::pow(firstValue[i]-secondValue[i],2);
            count++;
          }
      }
      else // dim == 3
      {
        for (coords[0] = bounds[0]+ 0.5*meshwidth; coords[0] < bounds[1]; coords[0] += meshwidth)
          for (coords[1] = bounds[2]+ 0.5*meshwidth; coords[1] < bounds[3]; coords[1] += meshwidth)
            for (coords[2] = bounds[4]+ 0.5*meshwidth; coords[2] < bounds[5]; coords[2] += meshwidth)
            {
              if (!firstVTK.eval(coords,firstValue))
                continue;
              if (!secondVTK.eval(coords,secondValue))
                continue;
              for (int i = 0; i < numComp; i++)
                sum += std::pow(firstValue[i]-secondValue[i],2);
            }
      }
      delete[] firstValue;
      delete[] secondValue;
      return std::sqrt(sum*volume);
    }

    void cell_residuals(std::string outName, std::string dataName, int dim) const
    {
      int numComp = firstVTK.getNumberOfComponents();
      if (numComp != 1)
        DUNE_THROW(Dune::NotImplemented,"plotting cell residuals is only implemented for scalar data");

      double count[3];
      double coords[3];
      std::vector<double> data;
      std::vector<double> firstData, secondData;
      for (int i = 0; i < 3; i++)
        coords[i] = bounds[2*i+1] - bounds[2*i];
      
      double firstValue[1];
      double secondValue[1];
      if (dim == 2)
      {
        count[2] = 1;
        count[1] = 0;
        for (coords[1] = bounds[2]+ 0.5*meshwidth; coords[1] < bounds[3]; coords[1] += meshwidth)
        {
          count[1]++;
          count[0] = 0;
          for (coords[0] = bounds[0]+ 0.5*meshwidth; coords[0] < bounds[1]; coords[0] += meshwidth)
          {
            count[0]++;
            if (!firstVTK.eval(coords,firstValue))
              data.push_back(0.);
            else if (!secondVTK.eval(coords,secondValue))
              data.push_back(0.);
            else
              data.push_back(firstValue[0]-secondValue[0]);

            firstData.push_back(firstValue[0]);
            secondData.push_back(secondValue[0]);
          }
        }
      }
      else // dim == 3
      {
        count[0] = 0;
        for (coords[0] = bounds[0]+ 0.5*meshwidth; coords[0] < bounds[1]; coords[0] += meshwidth)
        {
          count[0]++;
          count[1] = 0;
          for (coords[1] = bounds[2]+ 0.5*meshwidth; coords[1] < bounds[3]; coords[1] += meshwidth)
          {
            count[1]++;
            count[2] = 0;
            for (coords[2] = bounds[4]+ 0.5*meshwidth; coords[2] < bounds[5]; coords[2] += meshwidth)
            {
              count[2]++;
              if (!firstVTK.eval(coords,firstValue))
                data.push_back(0.);
              else if (!secondVTK.eval(coords,secondValue))
                data.push_back(0.);
              else
                data.push_back(firstValue[0]-secondValue[0]);

              firstData.push_back(firstValue[0]);
              secondData.push_back(secondValue[0]);
            }
          }
        }
      }

      VTKWriter writer;
      writer.writeData(outName,dataName,bounds,count,meshwidth,data);
    }
};

#endif // DUNE_VTKDIFF_VTKDIFF
