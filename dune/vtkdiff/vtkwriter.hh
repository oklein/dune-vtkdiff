#ifndef DUNE_VTKDIFF_VTKWRITER
#define DUNE_VTKDIFF_VTKWRITER

class VTKWriter
{
  public:

    VTKWriter()
    {}

    void writeData(std::string fileName, std::string dataName, const double bounds[], double count[],
        double meshwidth, const std::vector<double>& data)
    {
      std::ofstream file(fileName,std::ofstream::trunc);

      file << "# vtk DataFile Version 2.0" << std::endl
        << dataName << std::endl
        << "ASCII" << std::endl
        << "DATASET STRUCTURED_POINTS" << std::endl;
      file << "DIMENSIONS " << count[0] << " " << count[1] << " " << count[2] << std::endl;
      file << "ASPECT_RATIO " << meshwidth << " " << meshwidth << " " << meshwidth << std::endl;
      file << "ORIGIN " << bounds[0]+0.5*meshwidth << " " << bounds[2]+0.5*meshwidth
        << " " << bounds[4]+0.5*meshwidth << std::endl;
      file << "POINT_DATA " << data.size()      << std::endl
        << "SCALARS " << dataName << " float 1" << std::endl
        << "LOOKUP_TABLE default"               << std::endl;

      std::cout << "data.size: " << data.size() << std::endl;
      std::cout << "count: " << count[0] << " " << count[1] << " " << count[2] << std::endl;
      std::cout << "meshwidth: " << meshwidth << std::endl;

      for (unsigned int i = 0; i < data.size(); i++)
        file << data[i] << std::endl;

      file.close();
    }

};

#endif // DUNE_VISU_VTKWRITER
