# What is dune-vtkdiff?

dune-vtkdiff is a small application for comparison of VTK
output of simulation software packages. It was initially
written to compare the output of DORiE with that of muphi,
since it would have been significantly more complicated to
capture the output of the latter in the former for internal
comparison.

dune-vtkdiff requires the VTK package (tested versions: 7.1
and 8.1.1) to function. Please note that the comparison of
VTK output inherently introduces systematic errors, since
e.g. biquadratic or bicubic local ansatz functions have to
be converted to piecewise bilinear approximations. If this
is acceptable, dune-vtkdiff provides a quick and simple way
of comparing the output of different research codes.

# How to use dune-vtkdiff

dune-vtkdiff requires dune-common, but only for the build
process. The executable itself doesn't depend on the rest
of dune in any way.

dune-vtkdiff is built by putting its code and that of
dune-common into two subdirectories of an arbitrary folder, e.g.
*$HOME/dune*, and then executing
"dune-common/bin/dunecontrol --opts=\<optsFile\> all",
where \<optsFile\> is an option file like this:

```bash
# subdirectory to use as build-directory
BUILDDIR="$HOME/dune/releaseBuild"
# paths to external software in non-default locations
CMAKE_PREFIX_PATH="$HOME/software"
# options that control compiler verbosity
GXX_WARNING_OPTS="-Wall -pedantic"
# options that control compiler behavior
GXX_OPTS="-march=native -g -O3 -std=c++14"
```

The dune-vtkdiff executable expects at least eight arguments,
specifying a reference VTK file and an arbitrary number of
additional VTK files for comparison:
- the type of output ("norm" or "plot"), which either
computes the L2 norm of the difference of the VTK files or
writes out a VTK file containing the difference
- the data type of the reference VTK file ("vertex" or "cell")
- the data id of the reference VTK file (starting at zero)
- the file name of the reference VTK file
- the same three arguments repeated for each additional VTK
file that should be processed

# Where to get help

If you have problems with dune-vtkdiff that can't be solved
by looking at the implementation of the executable, check
the bug tracker at

https://gitlab.dune-project.org/oklein/dune-vtkdiff/issues

or contact the author directly:
* Ole Klein (ole.klein@iwr.uni-heidelberg.de)
