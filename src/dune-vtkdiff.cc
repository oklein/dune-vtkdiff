#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/common/exceptions.hh> // We use exceptions

#include <dune/vtkdiff/vtkreader.hh>
#include <dune/vtkdiff/vtkdiff.hh>

int main(int argc, char** argv)
{
  if (argc < 9)
  {
    std::cerr << "expected at least 8 arguments:\n"
      << "\tcommand (norm or plot) and meshwidth,\n"
      << "\tdata type, array id and filename of reference,\n"
      << "\tdata type, array id and filenames of vtk files" << std::endl;
    DUNE_THROW(Dune::Exception,"expected at least 8 arguments");
  }

  VTKReader reference(argv[5],argv[3],std::atoi(argv[4]));

  for (int i = 8; i < argc; i++)
  {
    try
    {
      VTKReader vtkfile(argv[i],argv[6],std::atoi(argv[7]));
      VTKDiff vtkDiff(reference,vtkfile,std::atof(argv[2]));
      if (std::string(argv[1]) == "norm")
        std::cout << "two_norm: " << vtkDiff.two_norm(2) << std::endl;
      else if (std::string(argv[1]) == "plot")
        vtkDiff.cell_residuals("residuals.vtk","residuals",2);
      else
        DUNE_THROW(Dune::Exception,"command " + std::string(argv[1]) + "not understood");
    }
    catch(Dune::Exception& e)
    {
      std::cout << "failed to treat filename: " << e << std::endl;
    }
    catch(std::exception& e)
    {
      std::cout << "failed to treat filename: " << e.what() << std::endl;
    }
  }

  return 0;
}
